package com.example.demo.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "students")
public class Student {
    @Id
    private long id;
    private String rollNumber;
    private String name;
    private int status;

    public enum Status{
        ACTIVE(1), DEACTIVE(0), DELETED(-1);
        int value;
        Status(int value){
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Status findByValue(int value){
            for (Status status: Status.values()) {
                if (status.getValue() == value){
                    return status;
                }
            }
            return null;
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        if (status == null) {
            status = Status.DEACTIVE;
        }
        this.status = status.getValue();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public static final class Builder {
        private long id;
        private String rollNumber;
        private String name;

        private Builder() {
        }

        public static Builder aStudent() {
            return new Builder();
        }

        public Builder withId(long id) {
            this.id = id;
            return this;
        }

        public Builder withRollNumber(String rollNumber) {
            this.rollNumber = rollNumber;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Student build() {
            Student student = new Student();
            student.setId(id);
            student.setRollNumber(rollNumber);
            student.setName(name);
            return student;
        }
    }
}
