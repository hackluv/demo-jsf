package com.example.demo.controller;

import com.example.demo.domain.Student;
import com.example.demo.model.StudentModel;

import javax.faces.bean.ManagedBean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@ManagedBean(name = "studentController", eager = true)
public class StudentController {
    private StudentModel model = new StudentModel();
    private Student st = new Student();
    private List<Student> students = new ArrayList<>();

    public Student getSt() {
        return st;
    }

    public void setSt(Student st) {
        this.st = st;
    }

    public List<Student> getAll(){
        students = model.getList();
        return students;
    }

    public String add(){
        this.st.setId(Calendar.getInstance().getTimeInMillis());
        this.st.setStatus(Student.Status.ACTIVE);
        this.model.saveOrUpdate(this.st);
        this.st = new Student();
        return "index";
    }

    public String update(Student st){
        this.st = st;
        return "edit";
    }

    public String update(){
        this.st.setStatus(Student.Status.ACTIVE);
        this.model.saveOrUpdate(this.st);
        return "index";
    }

    public void remove(Student st){
        this.st = st;
        model.delete(this.st.getId());
    }
}
