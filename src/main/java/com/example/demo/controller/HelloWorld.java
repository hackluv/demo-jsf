package com.example.demo.controller;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "helloWorld", eager = true)
public class HelloWorld {

    public HelloWorld() {
        System.out.println("com.example.demo.action.HelloWorld started!");
    }

    public String getMessage() {
        return "Hello World!";
    }
}