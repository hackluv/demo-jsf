package com.example.demo.model;

import com.example.demo.domain.Student;
import com.example.demo.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class StudentModel {

    public List<Student> getList(){
        List<Student> students = new ArrayList<>();
        List<Student> newList = new ArrayList<>();
        try (Session session = HibernateUtil.getSession()){
            students = session.createQuery("from Student", Student.class).list();
            for (Student st: students) {
                if (st.getStatus() != -1){
                    newList.add(st);
                }
            }
            return newList;
        }catch (Exception ex){
            ex.printStackTrace();
            return students;
        }
    }

    public void saveOrUpdate(Student student){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSession()) {
            transaction = session.beginTransaction();
            session.saveOrUpdate(student);
            transaction.commit();
        }catch (Exception ex){
            if (transaction != null){
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Student getById(long id){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSession()) {
            transaction = session.beginTransaction();
            Student student = session.get(Student.class, id);
            transaction.commit();
            return student;
        }catch (Exception ex){
            if (transaction != null){
                transaction.rollback();
            }
            ex.printStackTrace();
            return null;
        }
    }

    public void delete(long id){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSession()){
            transaction = session.beginTransaction();
            Student student = session.get(Student.class, id);
            if (student != null){
                student.setStatus(Student.Status.DELETED);
                session.update(student);
            }
            transaction.commit();
        }catch (Exception ex){
            if (transaction != null){
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
